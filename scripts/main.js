function changeColour() {
  const COLOUR_VALUE_WHITE = 'white';
  const COLOUR_VALUE_RED = 'red';
  const COLOUR_VALUE_GREEN = 'green';
  const COLOUR_VALUE_YELLOW = 'yellow';

  const COLOUR_CLASS_WHITE = 'bg-white';
  const COLOUR_CLASS_RED = 'bg-red';
  const COLOUR_CLASS_GREEN = 'bg-green';
  const COLOUR_CLASS_YELLOW = 'bg-yellow';

  let elColourSelect = document.getElementById('select-colour');
  let selectedColour = elColourSelect.value;
  if (selectedColour === COLOUR_VALUE_WHITE) {
    setTextareaClass(COLOUR_CLASS_WHITE);
  } else if (selectedColour === COLOUR_VALUE_RED) {
    setTextareaClass(COLOUR_CLASS_RED);
  } else if (selectedColour === COLOUR_VALUE_GREEN) {
    setTextareaClass(COLOUR_CLASS_GREEN);
  } else if (selectedColour === COLOUR_VALUE_YELLOW) {
    setTextareaClass(COLOUR_CLASS_YELLOW);
  }
}

// class Attribut von Textarea wird gesetzt
function setTextareaClass(className) {
  let elTextarea = document.getElementById('textarea');
  // class Attribut wird überschrieben mit neuem Wert className
  // => alle bisherigen Klassen in class Attribut werden gelöscht
  //    hier ok, da nur Klassen für Farben vorgesehen sind
  elTextarea.setAttribute('class', className);
}

function changeTextareaSize() {
  const SIZE_VALUE_SMALL = 'small';
  const SIZE_VALUE_BIG = 'big';

  const COLS_SMALL = 20;
  const COLS_BIG = 40;
  const ROWS_SMALL = 10;
  const ROWS_BIG = 20;

  let elSelectedSize = document.querySelector('#size-form input:checked');
  let selectedValue = elSelectedSize.value;
  if (selectedValue === SIZE_VALUE_BIG) {
    setTextareaColsRows(COLS_BIG, ROWS_BIG);
  } else if (selectedValue === SIZE_VALUE_SMALL) {
    setTextareaColsRows(COLS_SMALL, ROWS_SMALL);
  }
}

// set cols and rows attribute of textarea
function setTextareaColsRows(cols, rows) {
  let elTextarea = document.getElementById('textarea');
  elTextarea.setAttribute('cols', cols);
  elTextarea.setAttribute('rows', rows);
}
